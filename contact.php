<?php 
/*
Template name: contacts
*/
?>
<?php include 'include/header.php'; ?>
<?php include 'include/contactBubble.php'; ?>
<?php include 'include/nav.php' ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="scrollContent">
	<div class="contactPage clear">
		<div class="contactPage__contactLeftSection contactLeftSection">
			<div id="map"></div>
			<div class="pageWrapper clear">
				<div class="contactLeftSection__contInfo info">
				<?php  
				    if( have_rows('row') ):
					    while ( have_rows('row') ) : the_row(); ?> 
					<div class="info__infoRow infoRow clear">
						<p class="infoRow__title"><?php the_sub_field('title') ?></p>
						<p class="infoRow__text <?php echo get_sub_field('red') ? 'infoRow__text-red' : '' ?>"><?php echo wp_strip_all_tags( get_sub_field('text') ); ?></p>
					</div>				
				<?php	endwhile; endif; wp_reset_query(); ?>
				</div>
				<form action="#" type="post" class="form contacts_page">
					<div class="form-message success"><?php _e('Form submitted successfully', 'smartvent') ?></div>
					<div class="form-message failure"><?php _e('Form submition error', 'smartvent') ?></div>
					<input name="name" type="text" class="input name" placeholder="<?php _e('Name', 'smartvent') ?>">
					<input name="email" type="text" class="input email" placeholder="<?php _e('Email', 'smartvent') ?>">
					<input name="telephone" type="text" class="input telephone" placeholder="<?php _e('Telephone', 'smartvent') ?>">
					<textarea name="question" class="input quastion" rows="1" placeholder="<?php _e('Your question', 'smartvent') ?>"></textarea>
				</form>
				<input class="submit" type="submit" name="submit" value="<?php _e('send', 'smartvent') ?>" onclick="valid('contacts_page');">
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var coordinates = { lat:  <?php the_field('latitude', 58) ?>, lng: <?php the_field('longitude', 58) ?>};
function initMap() {
	// Specify features and elements to define styles.
	var styleArray = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}];
	// Create a map object and specify the DOM element for display.
	map = new google.maps.Map(document.getElementById('map'), {
		center: coordinates,
		scrollwheel: true,
		// Apply the map style array to the map.
		styles: styleArray,
		zoom: 10,
		//disableDefaultUI: true,
		draggable: true,
	});

	marker = new google.maps.Marker({
		map:map,
		position: new google.maps.LatLng(<?php the_field('pin_lat', 58) ?>,<?php the_field('pin_long', 58) ?>),
		icon: '<?php bloginfo("template_directory") ?>/images/pin.png'
	});
}
</script>
<?php endwhile; endif; ?>
<?php include 'include/footer.php'; ?>
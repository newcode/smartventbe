<?php
error_reporting(-1);

add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('smartvent', get_template_directory() . '/languages');
}

add_action('wp_ajax_getItemById', 'getItemById');
add_action('wp_ajax_nopriv_getItemById', 'getItemById');

function getItemById() {
  $post_id = $_GET['post_id'];
  $images_array = '';
  $the_query = new WP_Query(array('p' => $post_id)); 
    if($the_query->have_posts()) : while($the_query->have_posts()) : $the_query->the_post(); 
    if (get_field('youtube')) {
      $images_array = '<iframe src="https://www.youtube.com/embed/ujgQOosyIYk" frameborder="0" allowfullscreen></iframe>';
    } else {
      if( have_rows('featured_images') ):
      while ( have_rows('featured_images') ) : the_row(); 
        $image  = get_sub_field('image');
        $images_array .= '<div class="rightSection__img" style="background-image: url('.$image['url'].');"></div>';
      endwhile; endif;      
    };
    $return_html = '
    <div id="wrapper">
      <div class="scroll">'.$images_array.'</div>
      <div class="rightSection__arrow rightSection__arrow-left arrow">
        <div class="arrow__spike"></div>
      </div>
      <div class="rightSection__arrow rightSection__arrow-right arrow">
        <div class="arrow__spike arrow__spike-right"></div>
      </div>
    </div>
    <div class="rightSection__pageWrapper pageWrapper rightSectionWrapper">
      <span class="pageWrapper__date">'. wp_strip_all_tags(get_the_time('Y. m. d.')) . '</span>
      <h2 class="pageWrapper__articleTitle">' . 
      wp_strip_all_tags(get_the_title()) . '</h2>
      <p class="pageWrapper__text">'. wp_strip_all_tags(get_the_content()) . '</p></div>';
    endwhile; endif;
    print $return_html;
die();
}

add_action( 'init', 'create_post_types' );
function create_post_types() {
  register_post_type( 'hero',
    array(
      'labels' => array(
        'name' => __( 'Titulinis', 'smartvent' ),
        'featured_image' => 'Background image'
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', 'revisions'),
      'public' => true,
      'menu_icon' => 'dashicons-slides',
    'show_ui' => true,
    'featured_image' => true,
    )
  );
};

function to_slug($str, $options = array()) {
  // Make sure string is in UTF-8 and strip invalid UTF-8 characters
  $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());
  
  $defaults = array(
    'delimiter' => '-',
    'limit' => null,
    'lowercase' => true,
    'replacements' => array(),
    'transliterate' => false,
  );
  
  // Merge options
  $options = array_merge($defaults, $options);
  
  $char_map = array(
    // Latin
    'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C', 
    'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 
    'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O', 
    'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH', 
    'ß' => 'ss', 
    'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c', 
    'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 
    'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o', 
    'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th', 
    'ÿ' => 'y',
    // Latin symbols
    '©' => '(c)',
    // Greek
    'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
    'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
    'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
    'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
    'Ϋ' => 'Y',
    'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
    'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
    'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
    'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
    'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
    // Turkish
    'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
    'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g', 
    // Russian
    'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
    'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
    'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
    'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
    'Я' => 'Ya',
    'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
    'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
    'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
    'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
    'я' => 'ya',
    // Ukrainian
    'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
    'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
    // Czech
    'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U', 
    'Ž' => 'Z', 
    'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
    'ž' => 'z', 
    // Polish
    'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z', 
    'Ż' => 'Z', 
    'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
    'ż' => 'z',
    // Latvian
    'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N', 
    'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
    'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
    'š' => 's', 'ū' => 'u', 'ž' => 'z'
  );
  
  // Make custom replacements
  $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
  
  // Transliterate characters to ASCII
  if ($options['transliterate']) {
    $str = str_replace(array_keys($char_map), $char_map, $str);
  }
  
  // Replace non-alphanumeric characters with our delimiter
  $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
  
  // Remove duplicate delimiters
  $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
  
  // Truncate slug to max. characters
  $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
  
  // Remove delimiter from ends
  $str = trim($str, $options['delimiter']);
  
  return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
}


function custom_menu_page_removing() {
  remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action( 'admin_menu', 'custom_menu_page_removing' );

//Gets post cat slug and looks for single-[cat slug].php and applies it
add_filter('single_template', create_function(
  '$the_template',
  'foreach( (array) get_the_category() as $cat ) {
    if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
    return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
  return $the_template;' )
);

// Excerpt length
function custom_excerpt_length( $length ) {
  return 10;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
// more
function new_excerpt_more( $more ) {
  return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
//
add_theme_support( 'post-thumbnails' ); 

// clear default wordpress gallery stuff
add_filter( 'use_default_gallery_style', '__return_false' );

// Remove the admin bar from the front end
add_filter( 'show_admin_bar', '__return_false' );

//This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
   'primary'   => __( 'Top primary menu', 'newcode' ),
  ) );

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}

// Custom Walker - strip <li>
class Simple_Walker extends Walker
{
    public function walk( $elements, $max_depth )
    {
        $list = array ();

        foreach ( $elements as $item )

            $list[] = "<li class='navList__item" . implode($item->classes, ' ') ."'><a href='$item->url'>$item->title</a></li>";

        return join( "\n", $list );
    }
}

add_action( 'wp_ajax_contactform', 'contactform' );
add_action( 'wp_ajax_nopriv_contactform', 'contactform' );

function contactform() {

  $message_name = $_POST['name'];
  $phone = $_POST['telephone'];
  $email = $_POST['email'];
  $question = $_POST['quastion'];

  $message_text = '<html><body>';
  $message_text .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
  $message_text .= "<tr><td><strong>Name:</strong> </td><td>" . $message_name . "</td></tr>";
  $message_text .= "<tr><td><strong>Tel:</strong> </td><td>" . $phone . "</td></tr>";
  $message_text .= "<tr><td><strong>Email:</strong> </td><td>" . $email . "</td></tr>";
  $message_text .= "<tr><td><strong>Question:</strong> </td><td>" . $question . "</td></tr>";
  $message_text .= "</table>";
  $message_text .= "</body></html>";
  //php mailer variables
  $to = get_option('admin_email');
  $subject = "SmartVent question";
  $headers = "From: " . $to . "\r\n";
  $headers .= "Reply-To: ". $to . "\r\n";
  $headers .= "CC:". $to;
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=utf-8\r\n";
  $sent = wp_mail($to, $subject, $message_text, $headers);
  
  if ($sent) {
    print 1;
    die();  
  }
}

add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );


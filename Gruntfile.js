module.exports = function(grunt){

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass:{
			dist:{
				options:{
					style: 'expanded'
				},
				files:{
					'css/style.css' : 'sass/style.scss',
				}
			}
		},
		/*less: {
			development: {
				options: {
					paths: ['assets/css']
			   	},
				files: {
					'css/style.css': 'less/style.less'
				}
			}
		},*/
		//less
		/*watch:{
			css:{
				files: 'less/*.less',
				tasks: ['less']
			}
		},*/
		//sass
		watch:{
			css:{
				files: 'sass/*.scss',
				tasks: ['sass']
			}
		},
		/*cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
				'css/mincss/style.css': ['css/style.css'],
				}
			 }
		 },
 	 	/*uglify: {
			my_target: {
				files: {
					'dest/output.min.js': ['src/input1.js', 'src/input2.js']
				}
			}
		}*/

	});
	//installed plugins
	grunt.loadNpmTasks('grunt-contrib-sass');
	//grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	//grunt.loadNpmTasks('grunt-contrib-cssmin');
	//grunt.loadNpmTasks('grunt-contrib-uglify');
	//tasks
	grunt.registerTask('default', ['sass']);
	//grunt.registerTask('default', ['less']);
	grunt.registerTask('default', ['watch']);
	//grunt.registerTask('default', ['cssmin']);
	//grunt.registerTask('default', ['uglify']);
};

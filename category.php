<?php include 'include/header.php'; ?>
<?php include 'include/contactBubble.php'; ?>
<?php $cat_id = get_queried_object_id(); ?>
<?php $category = get_cat_name( $cat_id ); ?>
<?php include 'include/nav.php' ?>
<div class="scrollContent">
	<div class="innerPage clear">
		<div class="innerPage__leftSection leftSection">
			<div class="leftSection__pageWrapper pageWrapper">
				<h1 class="pageWrapper__title"><?php echo $category ?></h1>
				<div class="pageWrapper__grid grid">
					<?php include 'include/newsList.php'; ?>
				</div>
			</div>
		</div>
		<div class="innerPage__rightSection rightSection">
			<div class="article">
				<?php //include 'include/newsPost.php'; ?>
			</div>
		</div>
	</div>
</div>
<?php include 'include/footer.php'; ?>
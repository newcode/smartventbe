<?php 
	$i=0;
	$the_query = new WP_Query(array('cat' => $cat_id)); 
	if($the_query->have_posts()) : while($the_query->have_posts()) : $the_query->the_post(); 
		$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); 
	?>
	<div data-id="<?php the_ID(); ?>" class="grid__item item clear <?php echo $i==0 ? 'grid__item-active' : '' ?>">
		<div class="item__img" style="background-image: url(<?php echo $image[0]?> );"></div>
		<div class="item__rightItem rightItem">
			<span class="rightItem__date"><?php the_time('Y. m. d.'); ?></span>
			<p class="rightItem__text"><?php echo wp_strip_all_tags(get_the_excerpt()) ?></p>
		</div>
	</div>
	<?php $i++; endwhile; endif; ?>
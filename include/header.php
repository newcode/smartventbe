<!DOCTYPE html>
<!--[if IE 9]>
	<html class="ie9" lang="en-US">
<![endif]-->
<!--[if lt IE 9]>
	<html class="ie-old" lang="en-US">
<![endif]-->
<html>
      <head>
            <title><?php bloginfo('title'); ?></title>
            <meta charset="utf-8">
            <meta name="viewport" content="initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
            <meta name="format-detection" content="telephone=no" />
            <title></title>
            <link rel="stylesheet" href="<?php bloginfo('template_directory')?>/css/style.css">
            <link rel="icon" type="image/x-icon" href="<?php bloginfo('template_directory')?>/images/favicon.ico">
            <?php wp_head(); ?>
      </head>
      <body>
